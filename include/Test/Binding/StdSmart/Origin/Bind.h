/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Bind.h>
#include <Test/Token/Spec/Generic.h>
#include <Test/Token/Ownership.h>
#include <Test/Token/Multiplicity.h>
#include <Test/Token/Lifetime.h>
#include <Test/Token/Feature.h>

#include <Test/Binding/StdSmart/Origin/Basis.h>

#include <memory>

namespace Test
{

namespace Bind
{

namespace Internal
{

// ----- Itself ----------------------------------------------------------------
template <>
struct Itself<Test::Basis::StdSmart>
{
    using Type = std::in_place_t;
    static constexpr Type Value = std::in_place;
};

template <class Element>
struct ItselfType<Element, Test::Basis::StdSmart>
{
    using Type = std::in_place_type_t<Element>;
    static constexpr Type Value = std::in_place_type<Element>;
};
// ----- Itself ----------------------------------------------------------------

// ----- Instance --------------------------------------------------------------
template <class Element>
struct Instance<Element,
                Test::Basis::StdSmart,
                Test::Ownership::Unique,
                Test::Multiplicity::Optional>
{ using Type = std::unique_ptr<Element>; };

template <class Element>
struct Instance<Element,
                Test::Basis::StdSmart,
                Test::Ownership::Shared,
                Test::Multiplicity::Optional>
{ using Type = std::shared_ptr<Element>; };

template <class Element>
struct Instance<Element,
                Test::Basis::StdSmart,
                Test::Ownership::Weak,
                Test::Multiplicity::Optional>
{ using Type = std::weak_ptr<Element>; };
// ----- Instance --------------------------------------------------------------

namespace Rule
{

// ----- Spec ------------------------------------------------------------------
template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::StdSmart,
                                Test::Ownership::Unique,
                                Test::Multiplicity::Optional>>
    : TokenSpec<Token,
                Test::Basis::StdSmart,
                Test::Ownership::Unique,
                Test::Multiplicity::Optional> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::StdSmart,
                                Test::Ownership::Unique,
                                Test::Multiplicity::Single>>
    : TokenSpec<Token,
                Test::Basis::StdSmart,
                Test::Ownership::Unique,
                Test::Multiplicity::Single> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::StdSmart,
                                Test::Ownership::Shared,
                                Test::Multiplicity::Optional>>
    : TokenSpec<Token,
                Test::Basis::StdSmart,
                Test::Ownership::Shared,
                Test::Multiplicity::Optional> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::StdSmart,
                                Test::Ownership::Shared,
                                Test::Multiplicity::Single>>
    : TokenSpec<Token,
                Test::Basis::StdSmart,
                Test::Ownership::Shared,
                Test::Multiplicity::Single> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::StdSmart,
                                Test::Ownership::Weak,
                                Test::Multiplicity::Optional>>
    : TokenSpec<Token,
                Test::Basis::StdSmart,
                Test::Ownership::Weak,
                Test::Multiplicity::Optional> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::StdSmart,
                                Test::Ownership::Weak,
                                Test::Multiplicity::Single>>
    : TokenSpec<Token,
                Test::Basis::StdSmart,
                Test::Ownership::Weak,
                Test::Multiplicity::Single> {};
// ----- Spec ------------------------------------------------------------------

// ----- Lifetime --------------------------------------------------------------
template <>
struct Lifetime<Test::Basis::StdSmart, Test::Ownership::Unique>
{
    static constexpr auto Role        = Test::Lifetime::Role::Owner;
    static constexpr bool IsTrackable = false;
};

template <>
struct Lifetime<Test::Basis::StdSmart, Test::Ownership::Shared>
{
    static constexpr auto Role        = Test::Lifetime::Role::Owner;
    static constexpr bool IsTrackable = true;
};

template <>
struct Lifetime<Test::Basis::StdSmart, Test::Ownership::Weak>
{
    static constexpr auto Role        = Test::Lifetime::Role::Observer;
    static constexpr bool IsTrackable = true;
};

template <>
inline constexpr bool
IsLockable<Test::Basis::StdSmart, Test::Ownership::Weak> = true;

template <>
inline constexpr bool
IsExpirable<Test::Basis::StdSmart, Test::Ownership::Weak> = true;
// ----- Lifetime --------------------------------------------------------------

// ----- Access ----------------------------------------------------------------
template <>
inline constexpr bool
IsDereferenceable<Test::Basis::StdSmart, Test::Ownership::Weak> = false;

template <>
inline constexpr bool
IsProxyAccessible<Test::Basis::StdSmart, Test::Ownership::Weak> = true;
// ----- Access ----------------------------------------------------------------

// ----- IsPointerConstructible ------------------------------------------------
template <>
inline constexpr bool
IsPointerConstructible<Test::Basis::StdSmart, Test::Ownership::Unique> = true;

template <>
inline constexpr bool
IsPointerConstructible<Test::Basis::StdSmart, Test::Ownership::Shared> = true;

template <>
inline constexpr bool
IsPointerConstructible<Test::Basis::StdSmart, Test::Ownership::Weak> = false;
// ----- IsPointerConstructible ------------------------------------------------

// ----- IsItselfConstructible -------------------------------------------------
template <class Ownership>
inline constexpr bool
IsItselfConstructible<Test::Basis::StdSmart, Ownership> = false;
// ----- IsItselfConstructible -------------------------------------------------

// ----- Copy construction & assignment ----------------------------------------
// ----- StdSmart <- StdSmart --------------------------------------------------
// StdSmart::SharedAny<T> <- StdSmart::SomeAny<Y>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::StdSmart, Test::Ownership::Shared, TM,
        Test::Basis::StdSmart, Test::Ownership::Shared, YM> : ImplicitCopy {};

template <class TM,
          class YM>
struct
CanCopy<Test::Basis::StdSmart, Test::Ownership::Shared, TM,
        Test::Basis::StdSmart, Test::Ownership::Weak, YM> : ExplicitCopy
{
    using ExceptionType = std::bad_weak_ptr;
};

// StdSmart::WeakAny<T> <- StdSmart::SomeAny<Y>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::StdSmart, Test::Ownership::Weak, TM,
        Test::Basis::StdSmart, Test::Ownership::Shared, YM> : ImplicitCopy {};

template <class TM,
          class YM>
struct
CanCopy<Test::Basis::StdSmart, Test::Ownership::Weak, TM,
        Test::Basis::StdSmart, Test::Ownership::Weak, YM> : ImplicitCopy {};
// ----- StdSmart <- StdSmart --------------------------------------------------
// ----- Copy construction & assignment ----------------------------------------

// ----- Move construction & assignment ----------------------------------------
// ----- StdSmart <- StdSmart --------------------------------------------------
// StdSmart::UniqueAny<T> <- StdSmart::UniqueAny<Y>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::StdSmart, Test::Ownership::Unique, TM,
        Test::Basis::StdSmart, Test::Ownership::Unique, YM> : ImplicitMove {};

// StdSmart::SharedAny<T> <- StdSmart::SomeAny<Y>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::StdSmart, Test::Ownership::Shared, TM,
        Test::Basis::StdSmart, Test::Ownership::Unique, YM> : ImplicitMove {};

template <class TM,
          class YM>
struct
CanMove<Test::Basis::StdSmart, Test::Ownership::Shared, TM,
        Test::Basis::StdSmart, Test::Ownership::Shared, YM> : ImplicitMove {};

template <class TM,
          class YM>
struct
CanMove<Test::Basis::StdSmart, Test::Ownership::Shared, TM,
        Test::Basis::StdSmart, Test::Ownership::Weak, YM> : ExplicitCopy
{
    using ExceptionType = std::bad_weak_ptr;
};

// StdSmart::WeakAny<T> <- StdSmart::SomeAny<Y>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::StdSmart, Test::Ownership::Weak, TM,
        Test::Basis::StdSmart, Test::Ownership::Shared, YM> : ImplicitCopy {};

template <class TM,
          class YM>
struct
CanMove<Test::Basis::StdSmart, Test::Ownership::Weak, TM,
        Test::Basis::StdSmart, Test::Ownership::Weak, YM> : ImplicitMove {};
// ----- StdSmart <- StdSmart --------------------------------------------------
// ----- Move construction & assignment ----------------------------------------

// ----- IsOwnerBased ----------------------------------------------------------
template <>
inline constexpr bool
IsOwnerBased<Test::Basis::StdSmart, Test::Ownership::Shared> = true;

template <>
inline constexpr bool
IsOwnerBased<Test::Basis::StdSmart, Test::Ownership::Weak> = true;
// ----- IsOwnerBased ----------------------------------------------------------

} // namespace Rule

} // namespace Internal

} // namespace Bind

} // namespace Test
